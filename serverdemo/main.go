package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"
)

func main() {

	http.HandleFunc("/", index)
	http.HandleFunc("/admin", admin)
	http.HandleFunc("/dashboard", dashboard)
	http.HandleFunc("/profile", profile)
	http.HandleFunc("/greet/{name}", greet)
	http.ListenAndServe(":8080", nil)
	fmt.Println("Server is running on port 8080")
}

type Params struct {
	Date string
	Time string
}

type User struct {
	Name  string
	Email string
	Phone string
}

func greet(respWriter http.ResponseWriter, req *http.Request) {

}

func profile(respWriter http.ResponseWriter, req *http.Request) {
	userOne := User{
		Name:  "First User",
		Email: "first.user@mail.com",
		Phone: "0111111111111",
	}
	profileTemp, _ := template.ParseFiles("profile.html")
	profileTemp.Execute(respWriter, userOne)
}

func dashboard(resp http.ResponseWriter, req *http.Request) {

	data := Params{
		Date: time.Now().Format("01-01-2000"),
		Time: time.Now().Format("01:01:01"),
	}

	dashboardTemplate, _ := template.ParseFiles("dashboard.html")
	dashboardTemplate.Execute(resp, data)
}

func admin(resp http.ResponseWriter, req *http.Request) {
	loginTemplate, _ := template.ParseFiles("login.html")
	loginTemplate.Execute(resp, nil)
}
func index(resp http.ResponseWriter, req *http.Request) {
	// 1. print some content
	fmt.Fprintf(resp, "<h1> Welcome to my site </h1> <h4> More Content </h4>\n")

	// 2. load some html file

	// 3. load some html file and add some dynamic content

	// 4. load some html file and provide some content based on user parameter
}
