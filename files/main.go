package main

import (
	"fmt"
	"strconv"
	"strings"

	"ay.net/space/files/fileops"
)

func main() {
	// Files Ops
	// fileops.WriteTextFile("out.txt", "Hello from Goside")
	// fmt.Println("Data from file is: " + fileops.ReadTextFile("out.txt"))

	// JSON Ops
	//jsonops.Init()

	// Files Ops
	const scoresFiles = "highscore.txt"
	highscore := fileops.ReadTextFile(scoresFiles)
	fmt.Println("High Score is :" + highscore)
	scoreparts := strings.Split(highscore, ",")
	fmt.Println("Username is: " + scoreparts[0])
	fmt.Println("Got score of: " + scoreparts[1])

	currentUser := "superplayer"
	currentUsersScore := 10
	oldUserScore, _ := strconv.Atoi(scoreparts[1]) // "1" => 1 , "a"

	if currentUsersScore > oldUserScore {
		// update the file
		fileops.WriteTextFile(scoresFiles, currentUser+","+strconv.Itoa(currentUsersScore))
		fmt.Println("High Score Updated")
	}
}
