package fileops

import (
	"fmt"
	"io/ioutil"
	"os"
)

func CreateFile(p string) *os.File {
	fmt.Println("creating..")
	f, err := os.Create(p)
	if err != nil {
		panic(err)
	}
	return f
}
func WriteFile(f *os.File) {
	fmt.Println("writing")
	fmt.Fprintln(f, "data")
}

func closeFile(f *os.File) {
	fmt.Println("closing")
	err := f.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v\n", err)
		os.Exit(1)
	}
}
func ReadTextFile(filename string) string {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	return string(data)
}

func WriteTextFile(filename string, data string) {
	mydata := []byte(data)
	err := ioutil.WriteFile(filename, mydata, 0777)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}
