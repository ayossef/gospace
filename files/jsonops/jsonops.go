package jsonops

import (
	"encoding/json"
	"fmt"
	"strconv"
)

type Game struct {
	Username string `json:"username"`
	Score    int    `json:"score"`
}

func Init() {
	gameOne := Game{Username: "ahmed", Score: 29}
	fmt.Printf("%+v \n", gameOne)
	fmt.Println("Username:" + gameOne.Username)
	fmt.Println("Score:" + strconv.Itoa(gameOne.Score))

	bytes, err := json.Marshal(gameOne)
	if err != nil {
		panic(err)
	}
	fmt.Println("Data: " + string(bytes))
}
