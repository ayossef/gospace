package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

const PORT = "8080"

var list []string

func startServer() {
	router := mux.NewRouter()
	router.HandleFunc("/greet/{name}", greet).Methods("GET")
	http.ListenAndServe(":"+PORT, router)
}
func greet(respWriter http.ResponseWriter, req *http.Request) {

	username := extractItem("name", req)
	list = addToList(username)
	showUser(respWriter, username)
	showList(respWriter)

}

func addToList(username string) []string {
	return append(list, username) // stored XSS
}
func extractItem(key string, req *http.Request) string {
	reqParams := mux.Vars(req)
	var value = reqParams[key]
	return value
}
func showUser(respWriter http.ResponseWriter, username string) {
	respWriter.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(respWriter, "<h2>Welcome, "+username+"</h2>") // Reflected XSS
}
func showList(respWriter http.ResponseWriter) {
	fmt.Fprintf(respWriter, "<h4>List of users</h4>")
	for _, v := range list {
		fmt.Fprintf(respWriter, "<p>"+v+"<p>")
	}
}
func main() {
	startServer()
	fmt.Println("Server is running at port " + PORT)
}
