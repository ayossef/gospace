package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

func toJson(user User) []byte {
	jsonBytes, _ := json.Marshal(user)
	return jsonBytes
}

func fromJson(jsonBytes []byte) User {
	var data map[string]interface{}
	err := json.Unmarshal(jsonBytes, &data)
	if err != nil {
		panic(err)
	}
	id := data["id"].(string)
	name := data["name"].(string)
	email := data["email"].(string)

	user := User{ID: id, Name: name, Email: email}
	return user
}
func main() {
	user := User{ID: "1", Name: "User One", Email: "user@gmail.com"}
	fmt.Println("To json:" + string(toJson(user)))
	user = fromJson(toJson(user))
	fmt.Println("To json:" + string(toJson(user)))
}
