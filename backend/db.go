package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

const DBUSER = "dbamin"
const DBPASS = "adminpas"
const DBPORT = "3306"
const DBHOST = "localhost"
const DBNAME = "posts"

var dbConnection *sql.DB
var router *mux.Router
var err error

type Post struct {
	ID    string `json:"id"`
	Title string `json:"title"`
}

func dbConnect() {
	dbConnection, err = sql.Open("mysql", DBUSER+":"+DBPASS+"@tcp("+DBHOST+":"+DBPORT+")/"+DBNAME)
	if err != nil {
		panic(err)
	}
	fmt.Println("DB Connection Success")
}
func createRoutes() {
	router = mux.NewRouter()
	router.HandleFunc("/posts", getPosts).Methods("GET")
	// router.HandleFunc("/posts", createPost).Methods("POST")
	// router.HandleFunc("/posts/{id}", getPost).Methods("GET")
	// router.HandleFunc("/posts/{id}", updatePost).Methods("PUT")
	// router.HandleFunc("/posts/{id}", deletePost).Methods("DELETE")
}
func startServer() {
	http.ListenAndServe(":8091", router)
}
func main() {
	dbConnect()
	defer dbConnection.Close()

	createRoutes()
	startServer()
}
func getPosts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var posts []Post
	result, err := dbConnection.Query("SELECT id, title from posts")
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()
	for result.Next() {
		var post Post
		err := result.Scan(&post.ID, &post.Title)
		if err != nil {
			panic(err.Error())
		}
		posts = append(posts, post)
	}
	json.NewEncoder(w).Encode(posts)
}
