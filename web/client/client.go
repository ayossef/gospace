package client

import (
	"bufio"
	"fmt"
	"net/http"
)

func Url(url string) string {
	var respData string
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("Response status:", resp.Status)

	scanner := bufio.NewScanner(resp.Body)
	for i := 0; scanner.Scan(); i++ {
		respData += scanner.Text()
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}
	return respData
}
